package com.ff.calculatorapp.Utils

import java.math.MathContext
import java.math.RoundingMode

class CalculatorMain {

    var operatorList = arrayListOf<String>("+", "-", "X", "/")
    var chiffresList = arrayListOf<String>("1", "2", "3", "4", "5", "6", "7", "8", "9", "0")
    var firstValue = "0"
    var secondValue = ""
    var operator = ""
    var tmp = ""


    fun getTextFromPressedBtn(textButton: String): String {
        when (textButton) {
            in chiffresList -> addNumber(textButton)
            in operatorList -> addOperator(textButton)
            "." -> addDot(textButton)
            "CE" -> clear()
            "=" -> calculate()
            "RETURN" -> erase()
        }
        return ("$firstValue $operator $secondValue")
    }

    private fun erase() {

    }

    private fun addOperator(textButton: String) {
        if (secondValue.isBlank())
            operator = textButton
        else {
            firstValue = calculate()
            operator = textButton
        }
    }

    private fun addNumber(textButton: String) {
        if (operator.isBlank()) {
            if (firstValue == "0" && textButton == "0") firstValue =
                firstValue
            else if (firstValue == "0")
                firstValue = textButton
            else if (tmp == firstValue)
                firstValue = textButton
            else
                firstValue += textButton
        } else
            secondValue += textButton

    }

    private fun addDot(textButton: String) {
        if (operator.isNotBlank() && !secondValue.contains(textButton))
            secondValue += textButton
        else if (operator.isBlank() && !firstValue.contains(textButton))
            firstValue += textButton
    }

    private fun clear() {
        firstValue = "0"
        secondValue = ""
        operator = ""
    }

    private fun calculate(): String {

        if (operator.isNotBlank() && secondValue.isNotBlank()) {
            val mathContext = MathContext(10,RoundingMode.HALF_EVEN)
            val firstDeciValue = firstValue.toBigDecimal()
            val secondDeciValue = secondValue.toBigDecimal()

            try {
                when (operator) {

                    // Sum
                    operatorList[0] -> {
                        firstValue = (firstDeciValue + secondDeciValue).stripTrailingZeros().toString()
                    }

                    // Subtraction
                    operatorList[1] -> {
                        firstValue = (firstDeciValue - secondDeciValue).stripTrailingZeros().toString()
                    }

                    // Multiplication
                    operatorList[2] -> {
                        firstValue = firstDeciValue.multiply(secondDeciValue,mathContext).stripTrailingZeros().toPlainString()
                    }

                    // Division
                    operatorList[3] -> {
                        firstValue = firstDeciValue.divide(secondDeciValue,mathContext).stripTrailingZeros().toPlainString()
                    }

                }
            } catch (e: Exception) {
                firstValue = "Erreur !"
                e.printStackTrace()
            }
            secondValue = ""
            operator = ""
            tmp = firstValue
        }
        return firstValue
    }

}