package com.ff.calculatorapp.Utils

import java.math.BigDecimal
import kotlin.math.log

class Calculator {

    var display = ""
    var operatorList = arrayListOf<String>("+", "-", "X", "/")
    var chiffresList = arrayListOf<String>("1", "2", "3", "4", "5", "6", "7", "8", "9", "0")
    var firstValue = 0.toBigDecimal()
    var secondValue = 0.toBigDecimal()

    var result = 0.toBigDecimal()
    var operator: String = ""
    var countOperator: Int = 0
    var countDotFV: Int = 0
    var countDotSV: Int = 0
    var operatorPosition: Int = 0


    fun getTextFromPressedBtn(textButton: String): String {

        when (textButton) {

            in operatorList -> {

                checkDuplicatedOperator(textButton)


                if (countOperator == 0) {
                    countOperator++
                    firstValue = display.substring(0, display.length - 1).toBigDecimal()
                    operator = textButton
                } else if (countOperator >= 1) {
                    secondValue = display.substring(firstValue.toString().length + 1,display.length - 1)
                        .toBigDecimal()
                    println("secondValue : ${display.substring(firstValue.toString().length + 1,display.length - 1)}")
                    display = calculer() + textButton
                    operator = textButton
                }

            }
            "." -> removeDot(textButton)
            "CE" -> reInitialize()
            in chiffresList -> ifChiffrePressed(textButton)
        }
        return display
    }

    private fun calculer(): String {
        when (operator) {
            operatorList.get(0) -> {result = firstValue + secondValue}
            operatorList.get(1) -> {result = firstValue - secondValue}
            operatorList.get(2) -> {result = firstValue * secondValue}
            operatorList.get(3) -> {result = firstValue / secondValue}
        }
        firstValue = result
        return result.toString()
    }

    private fun checkDuplicatedOperator(textButton: String) {
        if (display.takeLast(1) in operatorList)
            display = display
        else
            display += textButton
    }

    private fun ifChiffrePressed(textButton: String) {
        if (display == "0") {
            display = textButton
        } else {
            display += textButton
        }
    }

    private fun removeDot(textButton: String) {
        if (countDotFV == 0) {
            countDotFV++
            display += textButton
        } else {
            display = display
        }
    }

    private fun reInitialize() {
        display = "0"
        firstValue = 0.toBigDecimal()
        secondValue = 0.toBigDecimal()
        countDotFV = 0
        countDotSV = 0
    }


}